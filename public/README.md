# Informations sur le jeu

## Auteur

St&eacute;phane Janvier

## Version

1.0.0

## Date

2020-03-05

## Type de jeu

Plate-forme

## But du jeu

R&eacute;ussir &agrave; attraper le drapeau &agrave; la fin de chaque tableau
